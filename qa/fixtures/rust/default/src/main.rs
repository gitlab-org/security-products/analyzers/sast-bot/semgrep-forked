use openssl::ssl::{SslConnectorBuilder, SslMethod, SSL_VERIFY_NONE};

fn main() {
    let mut connector = SslConnectorBuilder::new(SslMethod::tls()).unwrap();

    connector.builder_mut().set_verify(SSL_VERIFY_NONE);
}
